package me.koenn.updater.client;

import mkremins.fanciful.FancyMessage;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import static org.bukkit.ChatColor.*;

public class Updater {

    private static Updater updater;
    private Plugin plugin;
    private String MESSAGE_FORMAT = "&8[&7Gravestones&8] &aThere's a new update available! ";
    private String downloadLink;
    private String latestVersion;

    /**
     * Constructor for the Updater.
     * Automaticly checks for a new update, and notifies you if there is one.
     *
     * @param plugin       Plugin to check the updates for
     * @param downloadLink Download link of the plugin
     * @param pluginId     The Spigot plugin id of the plugin
     */
    public Updater(Plugin plugin, String downloadLink, String pluginId) {
        try {
            updater = this;
            //Set object variables.
            this.plugin = plugin;
            this.downloadLink = downloadLink;

            //Check if there is a new version.
            if (checkVersion(pluginId)) {

                //If you're running the latest version, send a message.
                this.plugin.getLogger().info("You are running the latest version!");
            } else {

                //Check if the connection was successful.
                if (latestVersion != null && !latestVersion.equals("null")) {

                    //If it is, send the update available message.
                    this.plugin.getLogger().info("There is a new update available (" + latestVersion + ")!\nUpdate here: " + downloadLink);
                    plugin.getServer().getOnlinePlayers().stream().filter(Player::isOp).forEach(this::sendUpdateMessage);
                } else {

                    //If it wasn't able to connect, send an error message.
                    throw new NullPointerException("Connection is invalid");
                }
            }
        } catch (Exception ex) {

            //If any error occurs, send an error message.
            this.plugin.getLogger().info("Failed to connect to the version server, not able to check for updates!");
        }
    }

    public Updater(Plugin plugin) {
        this.plugin = plugin;
    }

    public static Updater getUpdater() {
        return updater;
    }

    /**
     * Send the update available message to a player.
     *
     * @param player Player object
     */
    public void sendUpdateMessage(Player player) {
        new FancyMessage(translateAlternateColorCodes('&', this.MESSAGE_FORMAT))
                .then("Download it here.")
                .color(GOLD)
                .style(BOLD)
                .link(this.downloadLink)
                .tooltip("Click to go to the download page!")
                .send(player);
    }

    /**
     * Check if there is a new version of the plugin available.
     *
     * @param pluginId The Spigot plugin id.
     * @return true if you're running the latest version.
     */
    public boolean checkVersion(String pluginId) throws IOException {
        //Send update message.
        this.plugin.getLogger().info("Checking for updates...");

        //Set the latest version to null.
        this.latestVersion = null;

        //Connect to the spigot api page.
        HttpURLConnection con = (HttpURLConnection) new URL("http://www.spigotmc.org/api/general.php").openConnection();

        //Create the plugin request.
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.getOutputStream().write(("key=98BE0FE67F88AB82B4C197FAF1DC3B69206EFDCC4D3B80FC83A00037510B99B4&resource=" + pluginId).getBytes("UTF-8"));

        //Read the latest version String.
        this.latestVersion = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();

        //Return true if the version is equal to the current one.
        return this.latestVersion != null && this.latestVersion.length() <= 7 && this.latestVersion.equalsIgnoreCase(this.plugin.getDescription().getVersion());
    }

    /**
     * Send an Exception to the VersionServer.
     *
     * @param exception Exception to send
     */
    public void sendError(Exception exception) {
        this.plugin.getLogger().info("Sending error to mod author...");
        try {
            TCPClient.sendToServer("ERROR%" + this.plugin.getName() + "%" + this.plugin.getDescription().getVersion() + "%" + exception.toString() + "%" + this.plugin.getServer().getBukkitVersion() + "%" + Arrays.toString(exception.getStackTrace()));
        } catch (IOException ex) {
            this.plugin.getLogger().info("Failed to connect to the server, not able send error!");
            exception.printStackTrace();
        }
        this.plugin.getLogger().info("Successfully send error to mod author!");
    }
}
