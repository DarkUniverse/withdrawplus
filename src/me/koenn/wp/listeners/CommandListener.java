package me.koenn.wp.listeners;

import me.koenn.wp.WithdrawPlus;
import me.koenn.wp.util.ConfigManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandListener implements Listener {

    private String moneyCommand;
    private String expCommand;

    public CommandListener() {
        moneyCommand = ConfigManager.getString("MONEY_WITHDRAW_COMMAND", "commands");
        expCommand = ConfigManager.getString("EXP_WITHDRAW_COMMAND", "commands");
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
        Player player = e.getPlayer();
        String command = e.getMessage().split(" ")[0];
        String[] cmd = e.getMessage().split(" ");
        String[] args;
        if (cmd.length == 1) {
            args = new String[0];
        } else {
            args = new String[]{cmd[1]};
        }
        if (command.equalsIgnoreCase(moneyCommand)) {
            WithdrawPlus.getCommandHandler().onCommand(player, WithdrawPlus.getPlugin().getCommand("withdrawMoney"), null, args);
            e.setCancelled(true);
        } else if (command.equalsIgnoreCase(expCommand)) {
            WithdrawPlus.getCommandHandler().onCommand(player, WithdrawPlus.getPlugin().getCommand("withdrawXp"), null, args);
            e.setCancelled(true);
        }
    }
}
