package me.koenn.wp.commands.commands;

import me.koenn.wp.commands.WithdrawCommand;
import me.koenn.wp.util.ConfigManager;
import me.koenn.wp.util.VoucherType;
import me.koenn.wp.voucher.ExpVoucher;
import me.koenn.wp.voucher.MoneyVoucher;
import me.koenn.wp.voucher.Voucher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GetVoucher implements WithdrawCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length < 2) {
            return false;
        }
        Player target;
        if (args.length == 3) {
            try {
                target = Bukkit.getPlayer(args[2]);
            } catch (Exception ex) {
                return false;
            }
        } else if (args.length < 3) {
            if (sender instanceof Player) {
                target = (Player) sender;
            } else {
                return false;
            }
        } else {
            return false;
        }
        if (target == null) {
            return false;
        }
        VoucherType type;
        try {
            type = VoucherType.valueOf(args[0].toUpperCase());
        } catch (Exception ex) {
            return false;
        }
        int amount;
        try {
            amount = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            return false;
        }
        Voucher voucher;
        switch (type) {
            case EXP:
                voucher = new ExpVoucher(amount, ConfigManager.getString("defaultOwnerName", "misc"));
                break;
            case MONEY:
                voucher = new MoneyVoucher(amount, ConfigManager.getString("defaultOwnerName", "misc"));
                break;
            default:
                throw new NullPointerException("No valid voucher type");
        }
        target.getInventory().addItem(voucher.getVoucher());
        return true;
    }

    @Override
    public String getName() {
        return "givevoucher";
    }

    @Override
    public String getUsage() {
        return "/givevoucher <exp/money> <amount> [player]";
    }
}
