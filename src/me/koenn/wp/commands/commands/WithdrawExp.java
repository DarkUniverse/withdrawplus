package me.koenn.wp.commands.commands;

import me.koenn.wp.commands.WithdrawCommand;
import me.koenn.wp.util.ConfigManager;
import me.koenn.wp.util.ExpUtil;
import me.koenn.wp.util.MinMaxUtil;
import me.koenn.wp.util.SoundUtil;
import me.koenn.wp.voucher.ExpVoucher;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;

public class WithdrawExp implements WithdrawCommand {

    private String NOT_ENOUGH_XP;
    private String SUCCEED;
    private String MAX;
    private String MIN;

    public WithdrawExp() {
        NOT_ENOUGH_XP = translateAlternateColorCodes('&', ConfigManager.getString("NOT_ENOUGH_EXP", "commandMessages"));
        SUCCEED = translateAlternateColorCodes('&', ConfigManager.getString("EXP_WITHDRAW", "commandMessages"));
        MAX = translateAlternateColorCodes('&', ConfigManager.getString("EXP_MAX", "commandMessages"));
        MIN = translateAlternateColorCodes('&', ConfigManager.getString("EXP_MIN", "commandMessages"));
    }

    @Override
    public boolean execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            return false;
        }
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;
        int oldExp = ExpUtil.getTotalExperience(player);
        int exp;
        try {
            exp = Integer.valueOf(args[0]);
        } catch (NumberFormatException ex) {
            if (args[0].equalsIgnoreCase("all")) {
                exp = ExpUtil.getTotalExperience(player);
            } else {
                return false;
            }
        }
        if (exp > MinMaxUtil.getMaxExp()) {
            for (String message : MAX.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        if (exp < MinMaxUtil.getMinExp()) {
            for (String message : MIN.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        if (exp < 1) {
            return false;
        }
        if (oldExp < exp) {
            for (String message : NOT_ENOUGH_XP.split("%")) {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
            }
            return true;
        }
        ExpUtil.setTotalExperience(player, oldExp - exp);
        ExpVoucher voucher = new ExpVoucher(exp, player.getName());
        player.getInventory().addItem(voucher.getVoucher());
        for (String message : SUCCEED.replace("{amount}", String.valueOf(exp)).split("%")) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        }
        player.playSound(player.getLocation(), SoundUtil.getExpSound(), 1, 1);
        return true;
    }

    @Override
    public String getName() {
        return "withdrawXp";
    }

    @Override
    public String getUsage() {
        return ConfigManager.getString("EXP_USAGE", "commandMessages");
    }
}
