package me.koenn.wp.commands;

import me.koenn.wp.commands.commands.GetVoucher;
import me.koenn.wp.commands.commands.WithdrawExp;
import me.koenn.wp.commands.commands.WithdrawMoney;
import me.koenn.wp.util.ConfigManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

import static org.bukkit.ChatColor.translateAlternateColorCodes;

/**
 * Command handler class for the WithdrawPlus commands.
 */
public class CommandHandler implements CommandExecutor {

    private ArrayList<WithdrawCommand> commands = new ArrayList<>();

    /**
     * Gets called when a CommandSender executes a command.
     *
     * @param sender CommandSender object instance
     * @param cmd    Command object instance of executed command
     * @param label  String label of the executed command
     * @param args   String[] arguments of the executed command
     * @return boolean handled
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        //Loop through all commands and filter the current command.
        commands.stream().filter(command -> command.getName().equalsIgnoreCase(cmd.getName())).forEach(command -> {
            //Check if the sender has permission.
            if (!sender.hasPermission("withdrawplus." + command.getName().toLowerCase())) {
                //Send noPermission message.
                for (String message : ConfigManager.getString("NO_PERMISSION", "commandMessages").split("%")) {
                    sender.sendMessage(translateAlternateColorCodes('&', message));
                }
                return;
            }
            //Execute the command and check if its handled.
            if (!command.execute(sender, args)) {
                //Split the command usage and send it back to the sender.
                for (String message : command.getUsage().split("%")) {
                    sender.sendMessage(translateAlternateColorCodes('&', message));
                }
            }
        });
        return true;
    }

    /**
     * Setup all known commands.
     */
    public void setup() {
        commands.add(new WithdrawExp());
        commands.add(new WithdrawMoney());
        commands.add(new GetVoucher());
    }
}
