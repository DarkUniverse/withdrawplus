package me.koenn.wp.util;

import org.bukkit.Material;

/**
 * Utility class for the voucher items.
 */
public class ItemUtil {

    private static Material MONEY_MATERIAL;
    private static Material EXP_MATERIAL;

    /**
     * Setup the static Material variables for the voucher items.
     */
    public static void setupMaterials() {
        MONEY_MATERIAL = Material.valueOf(ConfigManager.getString("MONEY_ITEM", "items").toUpperCase());
        EXP_MATERIAL = Material.valueOf(ConfigManager.getString("EXP_ITEM", "items").toUpperCase());
    }

    //Getters for the static Material variables.

    public static Material getMoneyMaterial() {
        return MONEY_MATERIAL;
    }

    public static Material getExpMaterial() {
        return EXP_MATERIAL;
    }
}
