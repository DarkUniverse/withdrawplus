package me.koenn.wp.voucher;

import me.koenn.wp.util.ItemUtil;
import me.koenn.wp.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Voucher object for money.
 */
public class MoneyVoucher extends Voucher {

    /**
     * MoneyVoucher constructor.
     *
     * @param amount    Value of the voucher
     * @param ownerName Owner of the voucher
     */
    public MoneyVoucher(int amount, String ownerName) {
        super(ItemUtil.getMoneyMaterial(), "moneyVoucher", amount, ownerName);
    }

    /**
     * Get the value of the voucher from the ItemStack.
     *
     * @param item ItemStack to get the value from
     * @return int value of the voucher
     */
    public static int getAmountFromItem(ItemStack item) {
        int index = Util.getAmountIndexForMoneyVoucher();
        List<String> lore = item.getItemMeta().getLore();
        String amount = lore.get(index).split(" ")[Util.getAmountIndexForMoneyVoucherString()].replace("$", "");
        return Integer.parseInt(ChatColor.stripColor(amount));
    }
}
