package me.koenn.wp.voucher;

import me.koenn.wp.util.ItemUtil;
import me.koenn.wp.util.Util;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Voucher object for exp.
 */
public class ExpVoucher extends Voucher {

    /**
     * ExpVoucher constructor.
     *
     * @param amount    Value of the voucher
     * @param ownerName Owner of the voucher
     */
    public ExpVoucher(int amount, String ownerName) {
        super(ItemUtil.getExpMaterial(), "expVoucher", amount, ownerName);
    }

    /**
     * Get the value of the voucher from the ItemStack.
     *
     * @param item ItemStack to get the value from
     * @return int value of the voucher
     */
    public static int getAmountFromItem(ItemStack item) {
        int index = Util.getAmountIndexForXpVoucher();
        List<String> lore = item.getItemMeta().getLore();
        String amount = lore.get(index).split(" ")[Util.getAmountIndexForXpVoucherString()].toLowerCase().replace("xp", "");
        return Integer.parseInt(ChatColor.stripColor(amount));
    }
}
